//
//  SchoolsModel.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/13/23.
//

import Foundation

struct SchoolsModel: Codable {
    var schoolName: String
    var schoolCode: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case schoolCode = "dbn"
    }
}

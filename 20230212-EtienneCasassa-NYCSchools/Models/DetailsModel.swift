//
//  DetailsModel.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/13/23.
//

import Foundation

struct DetailsModel: Codable {
    var schoolName: String
    var schoolCode: String
    var numberOfTestTakers: String
    var mathAvgScore: String
    var readingAvgScore: String
    var writingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case schoolCode = "dbn"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case mathAvgScore = "sat_math_avg_score"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}

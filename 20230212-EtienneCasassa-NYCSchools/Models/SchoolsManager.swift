//
//  SchoolsManager.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/13/23.
//

import Foundation

class SchoolsManager {
    func parseSchoolsData(completion: @escaping ([SchoolsModel]) -> Void) {
        if let schoolURL = URL(string: Constants.schoolJSONURL) {
            URLSession.shared.dataTask(with: schoolURL) { (data, response, error) in
                if let safeSchoolData = data {
                    do {
                        let decodedSchoolsData = try JSONDecoder().decode([SchoolsModel].self, from: safeSchoolData)
                        DispatchQueue.main.async {
                            completion(decodedSchoolsData)
                        }
                    }
                    catch {
                        print("^^^^ Problem parsing Schools :( \(error)")
                    }
                }
            }.resume()
        }
    }
}

//
//  DetailsManager.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/13/23.
//

import Foundation

class DetailsManager {
    func parseDetailsData(completion: @escaping ([DetailsModel]) -> Void) {
        if let detailsURL = URL(string: Constants.detailJSONURL) {
            URLSession.shared.dataTask(with: detailsURL) { (data, response, error) in
                if let safeDetailsData = data {
                    do {
                        let decodedDetailsData = try JSONDecoder().decode([DetailsModel].self, from: safeDetailsData)
                        DispatchQueue.main.async {
                            completion(decodedDetailsData)
                        }
                    }
                    catch {
                        print("^^^^ Problem parsing Details :( \(error)")
                    }
                }
            }.resume()
        }
    }
}

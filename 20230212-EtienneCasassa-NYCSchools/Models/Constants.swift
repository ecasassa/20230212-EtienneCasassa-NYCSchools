//
//  Constants.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/12/23.
//

import Foundation

public class Constants {
    static let cellIdentifyer = "schoolNameCell"
    static let segueIdentifyer = "schoolsToDetails"
    static let schoolJSONURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let detailJSONURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

//
//  ActionPlan.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/12/23.
//

/*

 Current thoughts:
 - Only missing the Unit Tests
    - If I had more time, I would create a protocol for the API calls, and then implement it in the tests for each class. Mock the API calls and make place-holder JSON files that I could reach internally
    - Also check that the DetailsViewController labels are displaying the right strings. Same for the amount of cells and the text in the cells in the SchoolsViewController
 
 --------------------

My Action Plan

Breaking down the project:
 X 1. Create a project in gitlab. Do feature branches, and have clear commits.
 X 2. Make both API calls
    X decode the json files
    X and make sure the data is coming in
 X 3. Create the storyboard
    X I'll need a decent looking UX design that clearly displays the req fields
    X Home page is a TableView with school names
    X The TableView presents the DetailView for each respective row
    X Design the DetailView so it's clear. School name on top
        X Req fields: SAT scores for Math, Reading, and Writing
        X Add aditional info to display if I want
 X 4. Make sure the data comes in to the TableView
 X 5. Make it so the right school shows up in the DetailView
 X 6. MVC or MVVM? Make sure it follows one or the other
 7. Test it all
    X QA device and screeensizes
    X Found schools in SchoolsJSON that don't exist in DetailsJSON, and found that DetailsJSON has more schools than SchoolsJSON. Added to code to prevent the wrong data showing. And also found some schools with different names in both JSON files, but they have the same dbn (like the Bronx Collegiate Academy that turns into Bronx Expeditionary Learning High School)
    X Accessibility / VoiceOver
    - Do Unit Tests
 X 8. Beautify the UX. Probably ask the wife for help on looks and design... (I didn't end up asking my wife for her opinion if that wasn't clear...)

 --------------------
 
 Extra thoughts:
 X Views: SwiftUI or Storyboards? Probably Storyboards. SwiftUI is easy, and bigger companies are more likely to work with complex Storyboards. I also have more experience with it.

 X TableView: A simple straight forward TableView. No need for a CollectionView.
    X School name and maybe the borough?
 
 X DetailView: Make it a modal or it's own standalone page with a back button?
    X Going with modal
    X Data available: school name, math, reading, writing, # of test takers.
 
 X Data:
    X Make sure to make a constants file
    X check for secure in https
    X only grab data they asked to display
    X fail the calls safely with guards and if lets
    X print out the errors
    X if I have time: if the site is down use the local json files
 
 - Test: Make Unit Tests for sure. Hopefully I have the time to get the code coverage high.
 
 X No 3rd party libraries required. So no need to use any.
 
 - Fun features to add: At the end, if there is time.
    - Filter by borough or alphabetical
    - Compare schools scores?
    - Add images of schools to the cells?
    - Maybe calculate the total average for all schools in that area and do a red/green thing in each school to see if they are above or below average?
    - Or maybe a "school overview" thing with some details about the school from the SchoolsJSON file. There is a lot of cool information.
 

*/

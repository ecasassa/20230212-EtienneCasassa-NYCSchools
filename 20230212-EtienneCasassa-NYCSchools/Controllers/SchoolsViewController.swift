//
//  ViewController.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/12/23.
//

import UIKit

class SchoolsViewController: UIViewController {

    var schoolsManager = SchoolsManager()
    var schoolsModel = [SchoolsModel]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listOfSchoolsTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolsManager.parseSchoolsData() { result in
            self.schoolsModel = result
            self.tableView.reloadData()
        }
        listOfSchoolsTitle.text = "List of Schools"
    }
}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schoolsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifyer, for: indexPath) as! TableViewCell
        cell.schoolNameLabel.text = schoolsModel[indexPath.row].schoolName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Constants.segueIdentifyer, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsVC = segue.destination as? DetailsViewController
        let selectedRow = tableView.indexPathForSelectedRow
        detailsVC?.schoolCodeFromSchoolsVC = schoolsModel[selectedRow?.row ?? 0].schoolCode
        tableView.deselectRow(at: selectedRow ?? [0], animated: true)
    }
}

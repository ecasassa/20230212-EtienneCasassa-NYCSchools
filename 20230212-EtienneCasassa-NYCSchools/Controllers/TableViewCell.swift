//
//  TableViewCell.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/12/23.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

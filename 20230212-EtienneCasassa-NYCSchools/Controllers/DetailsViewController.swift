//
//  DetailsViewController.swift
//  20230212-EtienneCasassa-NYCSchools
//
//  Created by Etienne Casassa on 2/13/23.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var detailsManager = DetailsManager()
    var detailsModel = [DetailsModel]()
    var schoolCodeFromSchoolsVC = ""
    
    @IBOutlet weak var schoolTitleLabel: UILabel!
    
    @IBOutlet weak var mathTitleLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    
    @IBOutlet weak var readingTitleLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    
    @IBOutlet weak var writingTitleLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    
    @IBOutlet weak var numberOfTestTakersLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        view.backgroundColor = randomizeBackgroundColor()
    }
    
    // when testing for Accessibility, I found that there is no easy way to get back to the tableView using VoiceOver. With more time I would create a back or dismiss modal button.
    // To dismiss the modal do a Two-finger scrub (move two fingers back and forth three times quickly, making a “z”). But it isn't clear or easy to do if you can't tell that you are in a modal!
    
    func setupUI() {
        detailsManager.parseDetailsData() { result in
            self.detailsModel = result
            
            for dataListDetail in result {
                if dataListDetail.schoolCode == self.schoolCodeFromSchoolsVC {
                    self.schoolTitleLabel.text = dataListDetail.schoolName
                    self.mathScoreLabel.text = dataListDetail.mathAvgScore
                    self.readingScoreLabel.text = dataListDetail.readingAvgScore
                    self.writingScoreLabel.text = dataListDetail.writingAvgScore
                    self.numberOfTestTakersLabel.text = "\(dataListDetail.numberOfTestTakers) students took the test"
                    
                    self.schoolTitleLabel.accessibilityLabel = "School name: \(dataListDetail.schoolName)"
                    self.mathScoreLabel.accessibilityLabel = "Math score is: \(dataListDetail.mathAvgScore)"
                    self.readingScoreLabel.accessibilityLabel = "Reading score is: \(dataListDetail.readingAvgScore)"
                    self.writingScoreLabel.accessibilityLabel = "Writing score is: \(dataListDetail.writingAvgScore)"
                    
                    return
                } else {
                    self.schoolTitleLabel.text = "Can't find test details for school dbn: \(self.schoolCodeFromSchoolsVC)"
                    self.mathScoreLabel.text = "--"
                    self.readingScoreLabel.text = "--"
                    self.writingScoreLabel.text = "--"
                    self.numberOfTestTakersLabel.text = "--"
                    
                    self.mathScoreLabel.accessibilityLabel = "Can't find Math score average for this school"
                    self.readingScoreLabel.accessibilityLabel = "Can't find Reading score average for this school"
                    self.writingScoreLabel.accessibilityLabel = "Can't find Writing score average for this school"
                }
            }
        }
        mathTitleLabel.text = "Math Score Average:"
        readingTitleLabel.text = "Reading Score Average:"
        writingTitleLabel.text = "Writing Score Average:"
    }
    
    // Randomizing backgrounnd color from a limited list of handpicked colors
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func randomizeBackgroundColor() -> UIColor {
        let colorsArray = ["f3a683","778beb", "f5cd79", "786fa6", "596275", "cf6a87"]
        
        var randomColor = ""
        
        randomColor = colorsArray.randomElement() ?? ""
        
        return hexStringToUIColor(hex: randomColor)
    }
}

// idealy I can implement something like the commented code below in setupUI instead of the loop I got to work above. But I couldn't get it to work so I will come back around if there is time.
// Also, related topic, this is running through the list over and over again. With more time I would look into the waste of resources. Right now, every time the user taps on a row, the app parses through the data agian. Not great. I think I should parse through it once, and keep it cached or something like that.

/*
 
if let dataListDetail = self.detailsModel.first(where: { $0.schoolCode == self.schoolCodeFromSchoolsVC }) {
    self.schoolTitleLabel.text = dataListDetail.schoolName
    self.mathScoreLabel.text = dataListDetail.mathAvgScore
    self.readingScoreLabel.text = dataListDetail.readingAvgScore
    self.writingScoreLabel.text = dataListDetail.writingAvgScore
    print("Got a schoolCode match!")
} else {
    print("Didn't hit a schoolCode match")
}

*/
